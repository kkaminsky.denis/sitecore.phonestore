﻿using System.Web.Optimization;
using Sitecore.Pipelines;

namespace PhoneStore.Website.Bundles
{
    public class BootstrapBundle : Sitecore.Mvc.Pipelines.Loader.InitializeRoutes
    {
        public override void Process(PipelineArgs args)
        {
            RegisterBundles(BundleTable.Bundles);
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/bootstrap.js")
                .Include(
                    "~/Scripts/jquery-3.0.0.js",
                    "~/Scripts/popper.min.js",
                    "~/Scripts/bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/bundles/bootstrap.css")
                .Include(
                    "~/Content/bootstrap.min.css"));
        }
    }
}