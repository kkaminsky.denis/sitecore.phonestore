﻿using System.Web.Optimization;
using Sitecore.Pipelines;

namespace PhoneStore.Website.Bundles
{
    public class BasePageBundle : Sitecore.Mvc.Pipelines.Loader.InitializeRoutes
    {
        public override void Process(PipelineArgs args)
        {
            RegisterBundles(BundleTable.Bundles);
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/basepage.js")
                .Include(
                    "~/Scripts/phonestore.website.js"));

            bundles.Add(new StyleBundle("~/bundles/basepage.css")
                .Include(
                    "~/Content/phonestore.website.css"));
        }
    }
}