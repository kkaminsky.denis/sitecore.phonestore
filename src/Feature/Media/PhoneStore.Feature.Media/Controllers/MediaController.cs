﻿using Glass.Mapper.Sc;
using PhoneStore.Feature.Media.Services.Contracts;
using PhoneStore.Feature.Media.ViewModels;
using PhoneStore.Foundation.Repository.Context;
using Sitecore.Mvc.Controllers;
using Sitecore.Mvc.Presentation;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoneStore.Feature.Media.Controllers
{
    public class MediaController : SitecoreController
    {
        private readonly IMediaService _mediaService;
        private readonly IContextWrapper _contextWrapper;
        private readonly IGlassHtml _glassHtml;

        public MediaController(IMediaService mediaService, IContextWrapper contextWrapper, IGlassHtml glassHtml)
        {
            _mediaService = mediaService;
            _contextWrapper = contextWrapper;
            _glassHtml = glassHtml;
        }

        public ViewResult ImageSlider()
        {
            var viewModel = new ImageSliderViewModel();
            if (!string.IsNullOrEmpty(RenderingContext.Current.Rendering.DataSource))
            {
                var contentItem = _mediaService.GetImageSliderContent(RenderingContext.Current.Rendering.DataSource);

                viewModel.Slides = contentItem.Slides.Select(x => new ImageSliderSlideViewModel
                {
                    Image = new HtmlString(_glassHtml.Editable(x, i => i.Image, new { @class = "d-block w-100" })),
                    Title = new HtmlString(_glassHtml.Editable(x, i => i.Title)),
                    Description = new HtmlString(_glassHtml.Editable(x, i => i.Description))
                }).ToList();
                var firstItem = viewModel.Slides.FirstOrDefault();
                if (firstItem != null)
                    firstItem.IsActive = true;
                viewModel.ParentGuid = contentItem.Id.ToString();
            }

            var parameterValue = _contextWrapper.GetParameterValue("Slide Interval in Milliseconds");
            if (int.TryParse(parameterValue, out var interval))
                viewModel.SlideInterval = interval;
            viewModel.IsInExperienceEditorMode = _contextWrapper.IsExperienceEditor;

            return View(viewModel);
        }
    }
}