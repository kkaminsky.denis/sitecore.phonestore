﻿using Sitecore.Pipelines;
using System.Web.Optimization;

namespace PhoneStore.Feature.Media.Bundles
{
    public class ImageSliderBundle : Sitecore.Mvc.Pipelines.Loader.InitializeRoutes
    {
        public override void Process(PipelineArgs args)
        {
            RegisterBundles(BundleTable.Bundles);
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/imageslider.css")
                .Include(
                    "~/Content/Media/image-slider.css"));
        }
    }
}