﻿using System.Collections.Generic;

namespace PhoneStore.Feature.Media.ViewModels
{
    public class ImageSliderViewModel
    {
        public List<ImageSliderSlideViewModel> Slides { get; set; }

        public int SlidesCount => Slides.Count;

        public bool HasSlides => SlidesCount > 0;

        public bool IsInExperienceEditorMode { get; set; }

        public string ParentGuid { get; set; }

        public int SlideInterval { get; set; }

        public bool IsSliderIntervalSet => SlideInterval > 0;

        public ImageSliderViewModel()
        {
            Slides = new List<ImageSliderSlideViewModel>();
        }
    }
}