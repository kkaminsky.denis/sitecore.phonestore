﻿using System.Web;

namespace PhoneStore.Feature.Media.ViewModels
{
    public class ImageSliderSlideViewModel
    {
        public HtmlString Image { get; set; }

        public HtmlString Title { get; set; }

        public HtmlString Description { get; set; }

        public bool IsActive { get; set; }
    }
}