﻿using PhoneStore.Foundation.Model;
using Glass.Mapper.Sc.Fields;

namespace PhoneStore.Feature.Media.Models.Contracts
{
    public interface IImageSliderSlide : ICmsEntity
    {
        Image Image { get; }

        string Title { get; }

        string Description { get; }
    }
}