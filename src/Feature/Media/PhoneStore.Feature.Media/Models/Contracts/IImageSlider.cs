﻿using PhoneStore.Foundation.Model;
using System.Collections.Generic;

namespace PhoneStore.Feature.Media.Models.Contracts
{
    public interface IImageSlider : ICmsEntity
    {
        IEnumerable<IImageSliderSlide> Slides { get; set; }
    }
}
