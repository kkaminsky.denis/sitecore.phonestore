﻿using PhoneStore.Feature.Media.Models.Contracts;
using PhoneStore.Foundation.Model;
using System.Collections.Generic;

namespace PhoneStore.Feature.Media.Models
{
    public class ImageSlider : CmsEntity, IImageSlider
    {
        public IEnumerable<IImageSliderSlide> Slides { get; set; }
    }
}