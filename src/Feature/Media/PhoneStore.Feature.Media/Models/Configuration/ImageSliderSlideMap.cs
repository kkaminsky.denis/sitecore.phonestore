﻿using Glass.Mapper.Sc.Maps;
using PhoneStore.Feature.Media.Models.Contracts;

namespace PhoneStore.Feature.Media.Models.Configuration
{
    public class ImageSliderSlideMap : SitecoreGlassMap<IImageSliderSlide>
    {
        public override void Configure()
        {
            Map(config =>
            {
                config.AutoMap();
                config.Id(f => f.Id);
                config.Field(f => f.Image).FieldName("Image");
                config.Field(f => f.Title).FieldName("Title");
                config.Field(f => f.Description).FieldName("Description");
            });
        }
    }
}