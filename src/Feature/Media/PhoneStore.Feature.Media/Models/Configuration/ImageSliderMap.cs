﻿using Glass.Mapper.Sc.Maps;
using PhoneStore.Feature.Media.Models.Contracts;

namespace PhoneStore.Feature.Media.Models.Configuration
{
    public class ImageSliderMap : SitecoreGlassMap<IImageSlider>
    {
        public override void Configure()
        {
            Map(config =>
            {
                config.AutoMap();
                config.Id(f => f.Id);
            });
        }
    }
}