﻿using PhoneStore.Foundation.Model;
using Glass.Mapper.Sc.Fields;
using PhoneStore.Feature.Media.Models.Contracts;

namespace PhoneStore.Feature.Media.Models
{
    public class ImageSliderSlide : CmsEntity, IImageSliderSlide
    {
        public Image Image { get; set; }
        
        public string Title { get; set; }

        public string Description { get; set; }
    }
}