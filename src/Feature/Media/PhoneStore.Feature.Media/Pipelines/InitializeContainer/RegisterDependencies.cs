﻿using PhoneStore.Feature.Media.Services;
using PhoneStore.Feature.Media.Services.Contracts;
using PhoneStore.Foundation.IoC.Pipelines.InitializeContainer;

namespace PhoneStore.Feature.Media.Pipelines.InitializeContainer
{
    public class RegisterDependencies
    {
        public void Process(InitializeContainerArgs args)
        {
            args.Container.Register<IMediaService, MediaService>();
        }
    }
}