﻿using PhoneStore.Feature.Media.Models.Contracts;

namespace PhoneStore.Feature.Media.Services.Contracts
{
    public interface IMediaService
    {
        IImageSlider GetImageSliderContent(string contentGuid);
    }
}
