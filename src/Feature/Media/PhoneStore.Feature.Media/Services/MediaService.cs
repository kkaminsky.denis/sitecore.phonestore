﻿using PhoneStore.Feature.Media.Models.Contracts;
using PhoneStore.Feature.Media.Services.Contracts;
using PhoneStore.Foundation.Repository.Content;

namespace PhoneStore.Feature.Media.Services
{
    public class MediaService : IMediaService
    {
        private readonly IContentRepository _repository;

        public MediaService(IContentRepository repository)
        {
            _repository = repository;
        }

        public IImageSlider GetImageSliderContent(string contentGuid)
        {
            var imageSlider = _repository.GetContentItem<IImageSlider>(contentGuid);
            imageSlider.Slides = _repository.GetChildren<IImageSliderSlide>(contentGuid);

            return imageSlider;
        }
    }
}