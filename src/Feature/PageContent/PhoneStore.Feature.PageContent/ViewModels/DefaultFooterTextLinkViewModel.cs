﻿using System.Web;

namespace PhoneStore.Feature.PageContent.ViewModels
{
    public class DefaultFooterTextLinkViewModel
    {
        public HtmlString Link { get; set; }
    }
}