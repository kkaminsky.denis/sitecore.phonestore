﻿using Sitecore.Data.Items;
using System.Collections.Generic;
using System.Linq;

namespace PhoneStore.Feature.PageContent.ViewModels
{
    public class DefaultFooterViewModel
    {
        public List<DefaultFooterTextLinkViewModel> TextLinks { get; set; }

        public List<Item> ImageLinks { get; set; }

        public bool HasTextLinks => TextLinks.Any();

        public bool HasImageLinks => ImageLinks.Any();

        public bool IsInExperienceEditorMode { get; set; }

        public string TextLinksFolderId { get; set; }

        public string ImageLinksFolderId { get; set; }

        public DefaultFooterViewModel()
        {
            TextLinks = new List<DefaultFooterTextLinkViewModel>();
            ImageLinks = new List<Item>();
        }
    }
}