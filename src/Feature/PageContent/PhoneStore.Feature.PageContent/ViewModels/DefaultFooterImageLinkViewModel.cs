﻿using System.Web;

namespace PhoneStore.Feature.PageContent.ViewModels
{
    public class DefaultFooterImageLinkViewModel
    {
        public HtmlString Link { get; set; }
        public HtmlString Image { get; set; }
    }
}