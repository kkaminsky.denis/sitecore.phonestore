﻿using System.Web.Optimization;
using Sitecore.Pipelines;

namespace PhoneStore.Feature.PageContent.Bundles
{

    public class DefaultFooterBundle : Sitecore.Mvc.Pipelines.Loader.InitializeRoutes
    {
        public override void Process(PipelineArgs args)
        {
            RegisterBundles(BundleTable.Bundles);
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/defaultfooter.css")
                .Include(
                    "~/Content/PageContent/default-footer.css"));
        }
    }
}