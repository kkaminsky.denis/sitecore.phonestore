﻿using System.Web.Optimization;
using Sitecore.Pipelines;

namespace PhoneStore.Feature.PageContent.Bundles
{
    public class DefaultHeaderBundle : Sitecore.Mvc.Pipelines.Loader.InitializeRoutes
    {
        public override void Process(PipelineArgs args)
        {
            RegisterBundles(BundleTable.Bundles);
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/defaultheader.css")
                .Include(
                    "~/Content/PageContent/default-header.css"));
        }
    }
}