﻿using PhoneStore.Feature.PageContent.Repositories;
using PhoneStore.Feature.PageContent.Repositories.Contracts;
using PhoneStore.Feature.PageContent.Services;
using PhoneStore.Feature.PageContent.Services.Contracts;
using PhoneStore.Foundation.IoC.Pipelines.InitializeContainer;

namespace PhoneStore.Feature.PageContent.Pipelines.InitializeContainer
{
    public class RegisterDependencies
    {
        public void Process(InitializeContainerArgs args)
        {
            args.Container.Register<IPageContentService, PageContentService>();
            args.Container.Register<IPageContentRepository, PageContentRepository>();
        }
    }
}