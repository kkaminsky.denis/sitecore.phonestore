﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Glass.Mapper.Sc;
using PhoneStore.Feature.PageContent.Services.Contracts;
using PhoneStore.Feature.PageContent.ViewModels;
using PhoneStore.Foundation.Repository.Context;
using Sitecore.Mvc.Presentation;

namespace PhoneStore.Feature.PageContent.Controllers
{
    public class PageContentController : Controller
    {
        private readonly IPageContentService _pageContentService;
        private readonly IContextWrapper _contextWrapper;
        private readonly IGlassHtml _glassHtml;

        public PageContentController(IPageContentService pageContentService, IContextWrapper contextWrapper, IGlassHtml glassHtml)
        {
            _pageContentService = pageContentService;
            _contextWrapper = contextWrapper;
            _glassHtml = glassHtml;
        }

        public ViewResult DefaultHeader()
        {
            return View();
        }

        public ViewResult DefaultFooter()
        {
            var viewModel = new DefaultFooterViewModel();
            if (!string.IsNullOrEmpty(RenderingContext.Current.Rendering.DataSource))
            {
                var contentItem = _pageContentService.GetDefaultFooterContent(RenderingContext.Current.Rendering.DataSource);

                viewModel.TextLinks = contentItem.TextLinks.Select(x => new DefaultFooterTextLinkViewModel
                {
                    Link = new HtmlString(_glassHtml.Editable(x, i => i.Link))
                }).ToList();
                viewModel.ImageLinks = contentItem.ImageLinks.ToList();
                viewModel.TextLinksFolderId = contentItem.TextLinksFolderId.ToString();
                viewModel.ImageLinksFolderId = contentItem.ImageLinksFolderId.ToString();
            }
            viewModel.IsInExperienceEditorMode = _contextWrapper.IsExperienceEditor;

            return View(viewModel);
        }
    }
}