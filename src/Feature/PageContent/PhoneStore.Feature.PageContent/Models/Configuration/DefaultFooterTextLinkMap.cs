﻿using Glass.Mapper.Sc.Maps;
using PhoneStore.Feature.PageContent.Models.Contracts;

namespace PhoneStore.Feature.PageContent.Models.Configuration
{
    public class DefaultFooterTextLinkMap : SitecoreGlassMap<IDefaultFooterTextLink>
    {
        public override void Configure()
        {
            Map(config =>
            {
                config.AutoMap();
                config.Id(f => f.Id);
                config.Field(f => f.Link).FieldName("Link");
            });
        }
    }
}