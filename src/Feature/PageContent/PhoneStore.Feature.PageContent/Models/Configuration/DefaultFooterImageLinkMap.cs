﻿using Glass.Mapper.Sc.Maps;
using PhoneStore.Feature.PageContent.Models.Contracts;

namespace PhoneStore.Feature.PageContent.Models.Configuration
{
    public class DefaultFooterImageLinkMap : SitecoreGlassMap<IDefaultFooterImageLink>
    {
        public override void Configure()
        {
            Map(config =>
            {
                config.AutoMap();
                config.Id(f => f.Id);
                config.Field(f => f.Image).FieldName("Image");
                config.Field(f => f.Link).FieldName("Link");
            });
        }
    }
}