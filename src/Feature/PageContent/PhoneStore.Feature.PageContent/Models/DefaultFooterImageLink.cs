﻿using Glass.Mapper.Sc.Fields;
using PhoneStore.Feature.PageContent.Models.Contracts;
using PhoneStore.Foundation.Model;

namespace PhoneStore.Feature.PageContent.Models
{
    public class DefaultFooterImageLink : CmsEntity, IDefaultFooterImageLink
    {
        public Link Link { get; set; }

        public Image Image { get; set; }
    }
}