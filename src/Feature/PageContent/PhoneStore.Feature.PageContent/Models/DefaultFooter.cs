﻿using System;
using System.Collections.Generic;
using PhoneStore.Feature.PageContent.Models.Contracts;
using PhoneStore.Foundation.Model;
using Sitecore.Data.Items;

namespace PhoneStore.Feature.PageContent.Models
{
    public class DefaultFooter : CmsEntity, IDefaultFooter
    {
        public Guid TextLinksFolderId { get; set; }

        public Guid ImageLinksFolderId { get; set; }

        public IEnumerable<IDefaultFooterTextLink> TextLinks { get; set; }

        public IEnumerable<Item> ImageLinks { get; set; }
    }
}