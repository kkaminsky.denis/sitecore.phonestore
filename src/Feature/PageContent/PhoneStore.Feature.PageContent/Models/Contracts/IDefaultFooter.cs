﻿using PhoneStore.Foundation.Model;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;

namespace PhoneStore.Feature.PageContent.Models.Contracts
{
    public interface IDefaultFooter : ICmsEntity
    {
        Guid TextLinksFolderId { get; set; }

        Guid ImageLinksFolderId { get; set; }

        IEnumerable<IDefaultFooterTextLink> TextLinks { get; set; }

        IEnumerable<Item> ImageLinks { get; set; }
    }
}
