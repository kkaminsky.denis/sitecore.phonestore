﻿using Glass.Mapper.Sc.Fields;
using PhoneStore.Foundation.Model;

namespace PhoneStore.Feature.PageContent.Models.Contracts
{
    public interface IDefaultFooterImageLink : ICmsEntity
    {
        Link Link { get; }
        Image Image { get; }
    }
}
