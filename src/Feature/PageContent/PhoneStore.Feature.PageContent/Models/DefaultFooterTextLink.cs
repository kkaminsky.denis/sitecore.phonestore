﻿using Glass.Mapper.Sc.Fields;
using PhoneStore.Feature.PageContent.Models.Contracts;
using PhoneStore.Foundation.Model;

namespace PhoneStore.Feature.PageContent.Models
{
    public class DefaultFooterTextLink : CmsEntity, IDefaultFooterTextLink
    {
        public Link Link { get; set; }
    }
}