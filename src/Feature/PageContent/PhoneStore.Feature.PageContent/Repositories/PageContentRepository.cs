﻿using System.Linq;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Mvc;
using PhoneStore.Feature.PageContent.Models.Contracts;
using PhoneStore.Feature.PageContent.Repositories.Contracts;
using Sitecore.Data;
using Sitecore.Diagnostics;

namespace PhoneStore.Feature.PageContent.Repositories
{    
    public class PageContentRepository : IPageContentRepository
    {
        private readonly IMvcContext _mvcContext;

        public PageContentRepository()
        {
            _mvcContext = new MvcContext();
        }

        public IDefaultFooter GetDefaultFooter(string contentGuid)
        {
            Assert.ArgumentNotNullOrEmpty(contentGuid, "contentGuid");
            var defaultFooter = _mvcContext.SitecoreService.Database.GetItem(ID.Parse(contentGuid));
            var result = _mvcContext.SitecoreService.GetItem<IDefaultFooter>(defaultFooter);
            var childrenItems = defaultFooter.GetChildren();

            var textLinksFolder = childrenItems.FirstOrDefault(x => x.TemplateID == ID.Parse("{0AF59E52-1FE3-4D36-82DD-B67176E08FCE}"));
            if (textLinksFolder != null)
            {
                result.TextLinksFolderId = textLinksFolder.ID.Guid;
                result.TextLinks = textLinksFolder.GetChildren()
                    .Select(c => _mvcContext.SitecoreService.GetItem<IDefaultFooterTextLink>(c))
                    .ToArray();
            }

            var imageLinksFolder = childrenItems.FirstOrDefault(x => x.TemplateID == ID.Parse("{396FC7A1-4436-4FC5-A878-815800B36FDF}"));
            if (imageLinksFolder != null)
            {
                result.ImageLinksFolderId = imageLinksFolder.ID.Guid;
                result.ImageLinks = imageLinksFolder.GetChildren().ToArray();
            }

            return result;
        }
    }
}