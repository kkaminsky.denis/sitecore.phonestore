﻿using PhoneStore.Feature.PageContent.Models.Contracts;

namespace PhoneStore.Feature.PageContent.Repositories.Contracts
{
    public interface IPageContentRepository
    {
        IDefaultFooter GetDefaultFooter(string contentGuid);
    }
}
