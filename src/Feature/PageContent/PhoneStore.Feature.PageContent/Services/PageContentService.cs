﻿using PhoneStore.Feature.PageContent.Models.Contracts;
using PhoneStore.Feature.PageContent.Repositories.Contracts;
using PhoneStore.Feature.PageContent.Services.Contracts;

namespace PhoneStore.Feature.PageContent.Services
{
    public class PageContentService : IPageContentService
    {
        private readonly IPageContentRepository _pageContentRepository;

        public PageContentService(IPageContentRepository pageContentRepository)
        {
            _pageContentRepository = pageContentRepository;
        }

        public IDefaultFooter GetDefaultFooterContent(string contentGuid)
        {
            var defaultFooter = _pageContentRepository.GetDefaultFooter(contentGuid);

            return defaultFooter;
        }
    }
}