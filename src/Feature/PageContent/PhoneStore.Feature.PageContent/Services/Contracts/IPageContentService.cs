﻿using PhoneStore.Feature.PageContent.Models.Contracts;

namespace PhoneStore.Feature.PageContent.Services.Contracts
{
    public interface IPageContentService
    {
        IDefaultFooter GetDefaultFooterContent(string contentGuid);
    }
}
