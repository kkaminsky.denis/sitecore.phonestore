﻿using System;
using System.Linq;
using System.Web.Mvc;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using Sitecore.Pipelines;

namespace PhoneStore.Foundation.IoC.Pipelines.InitializeContainer
{
    public class InitializeContainer
    {
        public void Process(PipelineArgs args)
        {
            var container = new Container();
            container.Options.EnableAutoVerification = false;
            var containerArgs = new InitializeContainerArgs(container);
            CorePipeline.Run("initializeContainer", containerArgs);

            var assemblies = AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => a.FullName.StartsWith("PhoneStore.Feature.") ||
                            a.FullName.StartsWith("PhoneStore.Foundation."));
            container.RegisterMvcControllers(assemblies.ToArray());
            container.RegisterMvcIntegratedFilterProvider();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}