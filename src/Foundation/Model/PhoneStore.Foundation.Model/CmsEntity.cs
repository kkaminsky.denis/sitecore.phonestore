﻿using System;

namespace PhoneStore.Foundation.Model
{
    public class CmsEntity : ICmsEntity
    {
        public Guid Id { get; set; }
    }
}
