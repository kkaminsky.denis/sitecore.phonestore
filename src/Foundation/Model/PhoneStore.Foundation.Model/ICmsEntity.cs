﻿using System;

namespace PhoneStore.Foundation.Model
{
    public interface ICmsEntity
    {
        Guid Id { get; }
    }
}
