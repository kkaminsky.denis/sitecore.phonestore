﻿using PhoneStore.Foundation.IoC.Pipelines.InitializeContainer;
using PhoneStore.Foundation.Repository.Content;
using PhoneStore.Foundation.Repository.Context;

namespace PhoneStore.Foundation.Repository.Pipelines.InitializeContainer
{
    public class RegisterDependencies
    {
        public void Process(InitializeContainerArgs args)
        {
            args.Container.Register<IContentRepository, SitecoreContentRepository>();
            args.Container.Register<IContextWrapper, SitecoreContextWrapper>();
        }
    }
}