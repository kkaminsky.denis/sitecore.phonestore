﻿using PhoneStore.Foundation.Model;
using System.Collections.Generic;

namespace PhoneStore.Foundation.Repository.Content
{
    public interface IContentRepository
    {
        T GetContentItem<T>(string contentGuid) where T : class, ICmsEntity;
        IEnumerable<T> GetChildren<T>(string parentGuid) where T : class, ICmsEntity;
    }
}
