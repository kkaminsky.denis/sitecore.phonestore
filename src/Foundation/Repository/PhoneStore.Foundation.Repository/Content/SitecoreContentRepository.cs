﻿using System;
using System.Collections.Generic;
using System.Linq;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Mvc;
using PhoneStore.Foundation.Model;
using Sitecore.Data;
using Sitecore.Diagnostics;

namespace PhoneStore.Foundation.Repository.Content
{
    public class SitecoreContentRepository : IContentRepository
    {
        private readonly IMvcContext _mvcContext;

        public SitecoreContentRepository()
        {
            _mvcContext = new MvcContext();
        }

        public T GetContentItem<T>(string contentGuid) where T : class, ICmsEntity
        {
            Assert.ArgumentNotNullOrEmpty(contentGuid, "contentGuid");
            return _mvcContext.SitecoreService.GetItem<T>(Guid.Parse(contentGuid));
        }

        public IEnumerable<T> GetChildren<T>(string parentGuid) where T : class, ICmsEntity
        {
            Assert.ArgumentNotNullOrEmpty(parentGuid, "parentGuid");
            var parentItem = _mvcContext.SitecoreService.Database.GetItem(ID.Parse(parentGuid));
            var childrenItems = parentItem.GetChildren();

            if (childrenItems == null || childrenItems.Count == 0)
                return Enumerable.Empty<T>();
            return childrenItems.Select(c => _mvcContext.SitecoreService.GetItem<T>(c)).ToArray();
        }
    }
}