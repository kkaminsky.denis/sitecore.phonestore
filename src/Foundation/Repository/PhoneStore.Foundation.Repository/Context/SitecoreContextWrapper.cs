﻿using System.Linq;
using Sitecore.Mvc.Presentation;

namespace PhoneStore.Foundation.Repository.Context
{
    public class SitecoreContextWrapper : IContextWrapper
    {
        public bool IsExperienceEditor =>
            Sitecore.Context.PageMode.IsExperienceEditor;

        public string CurrentItemPath =>
            Sitecore.Context.Item.Paths.FullPath;

        public string GetParameterValue(string key)
        {
            var value = string.Empty;
            var parameters = RenderingContext.Current.Rendering.Parameters;
            if (parameters != null && parameters.Any())
                value = parameters[key];
            return value;
        }
    }
}