﻿namespace PhoneStore.Foundation.Repository.Context
{
    public interface IContextWrapper
    {
        string CurrentItemPath { get; }
        bool IsExperienceEditor { get; }
        string GetParameterValue(string key);
    }
}
