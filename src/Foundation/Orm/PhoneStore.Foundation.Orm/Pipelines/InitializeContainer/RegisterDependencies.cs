﻿using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Mvc;
using SimpleInjector;
using PhoneStore.Foundation.IoC.Pipelines.InitializeContainer;

namespace PhoneStore.Foundation.Orm.Pipelines.InitializeContainer
{
    public class RegisterDependencies
    {
        public void Process(InitializeContainerArgs args)
        {
            args.Container.Register<IMvcContext>(() => new MvcContext(), Lifestyle.Transient);
            args.Container.Register<ISitecoreService>(() => new SitecoreService(Sitecore.Context.Database), Lifestyle.Singleton);
            args.Container.Register<IGlassHtml, GlassHtml>();
        }
    }
}