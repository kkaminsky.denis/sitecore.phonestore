var gulp = require("gulp");
var msbuild = require("gulp-msbuild");
var debug = require("gulp-debug");
var foreach = require("gulp-foreach");
var gulpConfig = require("./gulp-config.js")();

module.exports.config = gulpConfig;

gulp.task("Publish-Site", function () {
    return gulp.src("./src/{Feature,Foundation,Project}/**/**/*.csproj")
        .pipe(foreach(function (stream, file) {
            return stream
                .pipe(debug({ title: "Publishing " }))
                .pipe(msbuild({
                    targets: ["Build"],
                    configuration: gulpConfig.buildConfiguration,
                    properties: {
                        publishUrl: gulpConfig.webRoot,
                        DeployDefaultTarget: "WebPublish",
                        WebPublishMethod: "FileSystem",
                        DeployOnBuild: "true",
                        DeleteExistingFiles: "false",
                        _FindDependencies: "false",
                        //VisualStudioVersion: "15.0"
                    },
                    toolsVersion: gulpConfig.MSBuildToolsVersion
                }));
        }));
});